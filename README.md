# Ud3_Ejemplo4
_Ejemplo 4 de la Unidad 3._ 

Vamos a pasar datos entre Actividades creando _Intents_ explícitos con datos extras. Para ello vamos a basarnos en el proyecto [Ud3_Ejemplo3](https://bitbucket.org/bitbucketcarlos/ud3_ejemplo3) y 
realizaremos los siguientes cambios (los demás ficheros no necesitarán cambios salvo el nombre del proyecto):

## _actividad1.xml_ y _actividad2.xml_
En este ejemplo los _TextViews_ de los _layouts_ de las actividades no tendrán texto asociado ya que será el que le pase la
actividad principal. Sí es necesario que tengan sus _ids_ para posteriormente buscarlos.

_actividad1.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Actividad1">

    <TextView
        android:id="@+id/TextViewAct1"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
_actividad2.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Actividad2">

    <TextView
        android:id="@+id/TextViewAct2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
## _MainActivity.kt_
En el fichero _MainActivity.kt_ deberemos añadir los datos extra que los _Intents_ pasarán:
```java
...
        /* Accedemos a los botones, les asignamos el Listener, asignamos los datos extra del Intent y lo lanzamos
         Con la función 'apply' aplicamos el contenido del bloque sobre el objeto creado. En este caso el 'putExtra'
         sobre el Intent creado*/
        binding.actividad1.setOnClickListener {
            val intentAct1 = Intent(this, Actividad1::class.java).apply {
                putExtra(Intent.EXTRA_TEXT,"Texto Actividad 1")
            }
            startActivity(intentAct1)
        }

        binding.actividad2.setOnClickListener {
            val intentAct2 = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT,"Texto Actividad 2")
            }
            startActivity(intentAct2)
        }
...
```
## _Actividad1.kt_ y _Actividad2.kt_
El último cambio que debemos hacer es recoger los datos enviados por la actividad principal a las otras dos actividades.
Para ello debemos comprobar si el _Intent_ recibido tiene datos extras y si es así incluirlos en el _TextView_ del _layout_ 
asociado a la actividad.
Para controlar que no nos llegue un texto con valor _null_, en la Actividad 1 asignaremos el texto "No hay  mensaje" utilizando el operador _?_ y en la Actividad 2 con la función _let_.

_Actividad1.kt_:
```java
class Actividad1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actividad1)

        // En el caso de que no se encuentre la clave del String se mostrará "No hay mensaje"
        val textoRecibido = intent.getStringExtra(Intent.EXTRA_TEXT) ?: "No hay mensaje"

        // Accedemos al View 'TextViewAct1' y le asignamos el texto recibido o el texto "No hay mensaje"
        findViewById<TextView>(R.id.TextViewAct1).apply {
            text = textoRecibido
        }
    }
}
```
_Actividad2.kt_:
```java
class Actividad2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actividad2)

        var textoRecibido = "No hay mensaje"

        // Si no es nulo cambiamos el valor a lo enviado utilizando 'let'
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            // Con 'it' accedemos al argumento que ha llamado a 'let', en este caso "Intent.EXTRA_TEXT"
            textoRecibido = it
        }

        // Accedemos al View 'TextViewAct1' y le asignamos el texto recibido o el texto "No hay mensaje"
        findViewById<TextView>(R.id.TextViewAct2).apply {
            text = textoRecibido
        }
    }
}
```
