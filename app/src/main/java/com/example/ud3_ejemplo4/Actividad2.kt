package com.example.ud3_ejemplo4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Actividad2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actividad2)

        var textoRecibido = "No hay mensaje"

        // Si no es nulo cambiamos el valor a lo enviado utilizando 'let'
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            // Con 'it' accedemos al argumento que ha llamado a 'let', en este caso "Intent.EXTRA_TEXT"
            textoRecibido = it
        }

        // Accedemos al View 'TextViewAct1' y le asignamos el texto recibido o el texto "No hay mensaje"
        findViewById<TextView>(R.id.TextViewAct2).apply {
            text = textoRecibido
        }
    }
}