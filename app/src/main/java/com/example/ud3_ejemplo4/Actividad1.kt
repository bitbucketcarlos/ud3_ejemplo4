package com.example.ud3_ejemplo4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Actividad1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actividad1)

        // En el caso de que no se encuentre la clave del String se mostrará "No hay mensaje"
        val textoRecibido = intent.getStringExtra(Intent.EXTRA_TEXT) ?: "No hay mensaje"

        // Accedemos al View 'TextViewAct1' y le asignamos el texto recibido o el texto "No hay mensaje"
        findViewById<TextView>(R.id.TextViewAct1).apply {
            text = textoRecibido
        }
    }
}